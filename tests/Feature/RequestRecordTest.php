<?php

namespace Domatskiy\Tests\Feature;

use Domatskiy\BeelineCloudPBX;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

/**
 * Class RequestRecordTest
 * @package Domatskiy\Tests\Feature
 */
class RequestRecordTest extends BeelineCloudPBX\Tests\Feature\FeatureTestCase
{
    /**
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function testGetRecords()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/records_200.json', 200),
        ]));

        $ServiceClient->setHandler($handler);

        $filter = new BeelineCloudPBX\Data\Records\Filter();
        $filter->setId(111);

        $result = $ServiceClient->getRecords($filter);
        foreach ($result as $record) {
            /**
             * @var $record BeelineCloudPBX\Response\Record
             */
            // $record->getId()
            $this->assertInstanceOf(BeelineCloudPBX\Response\Record::class, $record);
        }
    }

    public function testGetRecord()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/record_200.json', 200),
        ]));

        $ServiceClient->setHandler($handler);

        $filter = new BeelineCloudPBX\Data\Records\Filter();
        $filter->setId(111);

        $record = $ServiceClient->getRecord(32232);
        $this->assertInstanceOf(BeelineCloudPBX\Response\Record::class, $record);
    }

    public function testGetRecordLink()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/record_link_200.json', 200),
        ]));

        $ServiceClient->setHandler($handler);

        $filter = new BeelineCloudPBX\Data\Records\Filter();
        $filter->setId(111);

        $link = $ServiceClient->getRecordLink(32232);
        // $link->getUrl();
        $this->assertInstanceOf(BeelineCloudPBX\Response\DirectRefResponse::class, $link);
    }

    public function testRecordDownload()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/record_file.mp3', 200),
        ]));

        $ServiceClient->setHandler($handler);

        $filter = new BeelineCloudPBX\Data\Records\Filter();
        $filter->setId(111);

        $file = $ServiceClient->getRecordFile(32232);
    }
}
