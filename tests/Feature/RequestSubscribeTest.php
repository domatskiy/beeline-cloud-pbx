<?php

namespace Domatskiy\Tests\Feature;

use Domatskiy\BeelineCloudPBX;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

class RequestSubscribeTest extends BeelineCloudPBX\Tests\Feature\FeatureTestCase
{
    /**
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function testPutSubscribe()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/subsciption_add_200.json', 200),
        ]));

        $ServiceClient->setHandler($handler);

        /**
         * 200
         */
        $req = new BeelineCloudPBX\Data\Subscription\SubscriptionRequest();
        $req->setUrl('http://dsdssd.ru/ssss');
        $req->setPattern('89999787784');
        $req->setExpires(85555);
        $req->setSubscriptionType('GROUP');

        $result = $ServiceClient->addSubscription($req);

        $this->assertInstanceOf(BeelineCloudPBX\Response\SubscriptionResult::class, $result);
    }

    public function testGetSubscribe()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/subsciption_info_200.json', 200),
        ]));

        $ServiceClient->setHandler($handler);

        /**
         * 200
         */
        $result = $ServiceClient->getSubscription(23322332);

        $this->assertInstanceOf(BeelineCloudPBX\Response\SubscriptionInfo::class, $result);
    }

    public function testDelSubscribe()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/subsciption_info_200.json', 200),
        ]));

        $ServiceClient->setHandler($handler);

        /**
         * 200
         */
        $result = $ServiceClient->deleteSubscription(23322332);

        // $this->assertInstanceOf(BeelineCloudPBX\Response\SubscriptionInfo::class, $result);
    }
}
