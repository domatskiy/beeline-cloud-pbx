<?php

namespace Domatskiy\Tests\Feature;

use Domatskiy\BeelineCloudPBX;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

class RequestAbonentTest extends BeelineCloudPBX\Tests\Feature\FeatureTestCase
{
    /**
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function testGetAbonents()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/abonents_200.json', 200),
            // $this->getResponseFromFile(__DIR__.'/data/err422.json', 422),
        ]));

        $ServiceClient->setHandler($handler);

        /**
         * 200
         */
        $result = $ServiceClient->getAbonents();
        foreach ($result as $abonent) {
            $this->assertInstanceOf(BeelineCloudPBX\Response\Abonent::class, $abonent);
            $this->assertNotEmpty($abonent->getUserId());
            $this->assertNotEmpty($abonent->getPhone());
            // $this->assertNotEmpty($abonent->getEmail());
            $this->assertNotEmpty($abonent->getFirstName());
            $this->assertNotEmpty($abonent->getLastName());
            $this->assertNotEmpty($abonent->getExtension());
        }
    }

    public function testGetAbonent()
    {
        $ServiceClient = new BeelineCloudPBX(1111111);
        $handler = HandlerStack::create(new MockHandler([
            $this->getResponseFromFile(__DIR__.'/data/abonent_200.json', 200),
        ]));

        $ServiceClient->setHandler($handler);

        /**
         * 200
         */
        $abonent = $ServiceClient->getAbonent('9922265445');
        $this->assertInstanceOf(BeelineCloudPBX\Response\Abonent::class, $abonent);
        $this->assertEquals('9992224466@mpbx.sip.beeline.ru', $abonent->getUserId());
        $this->assertEquals('9992224466', $abonent->getPhone());
    }
}
