<?php

namespace Domatskiy\BeelineCloudPBX\Tests\Feature;

use Domatskiy\BeelineCloudPBX\Tests\TestCase;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

abstract class FeatureTestCase extends TestCase
{
    /**
     * @param $path
     * @param int $status
     *
     * @return HandlerStack
     * @throws \Exception
     */
    protected function getHandler($path, $status = 200)
    {
        $response = $this->getResponseFromFile($path, $status);

        // Create a mock and queue two responses.
        $mock = new MockHandler([
            $response
        ]);

        $handler = HandlerStack::create($mock);

        return $handler;
    }

    /**
     * @param $path
     * @param $status
     *
     * @return \GuzzleHttp\Psr7\Response
     * @throws \Exception
     */
    protected function getResponseFromFile($path, $status)
    {
        if ($status < 1) {
            throw new \Exception('not correct status');
        }

        $stream = file_get_contents($path);
        $pathParts = pathinfo($path);

        $ContentType = 'application/octet-stream;charset=utf-8';

        switch ($pathParts['extension']) {
            case 'json':
                $ContentType = 'application/json;charset=utf-8';
                break;
        }



        return new \GuzzleHttp\Psr7\Response($status, [
            'Content-Type' => $ContentType
        ], $stream);
    }

    protected function getResponseFromString($string, $status = 200)
    {
        $stream = fopen('data://text/plain;base64,' . base64_encode($string), 'r');
        $stream = stream_get_contents($stream);

        return new \GuzzleHttp\Psr7\Response($status, [], $stream);
    }
}
