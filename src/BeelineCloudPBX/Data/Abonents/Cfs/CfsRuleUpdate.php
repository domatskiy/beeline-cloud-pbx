<?php

namespace Domatskiy\BeelineCloudPBX\Data\Abonents\Cfs;

class CfsRuleUpdate
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $forwardToPhone;

    /**
     * (enum) = [
     *  ROUND_THE_CLOCK (Круглосуточно),
     *  WORKING_TIME (Рабочее время),
     *  NON_WORKING_TIME_AND_HOLIDAYS (Нерабочие часы и выходные)]
     * @var string
     */
    protected $schedule;

    /**
     * @var array
     */
    protected $phoneList;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getForwardToPhone(): string
    {
        return $this->forwardToPhone;
    }

    /**
     * @param string $forwardToPhone
     */
    public function setForwardToPhone(string $forwardToPhone): void
    {
        $this->forwardToPhone = $forwardToPhone;
    }

    /**
     * @return string
     */
    public function getSchedule(): string
    {
        return $this->schedule;
    }

    /**
     * @param string $schedule
     */
    public function setSchedule(string $schedule): void
    {
        $this->schedule = $schedule;
    }

    /**
     * @return array
     */
    public function getPhoneList(): array
    {
        return $this->phoneList;
    }

    /**
     * @param array $phoneList
     */
    public function setPhoneList(array $phoneList): void
    {
        $this->phoneList = $phoneList;
    }
}
