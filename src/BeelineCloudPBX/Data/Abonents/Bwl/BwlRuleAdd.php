<?php

namespace Domatskiy\BeelineCloudPBX\Data\Abonents\Bwl;

class BwlRuleAdd
{
    /**
     * (enum) = [
     *  BLACK_LIST (Не принимать звонки с указанных в списке правил номеров),
     *  WHITE_LIST (Принимать звонки только с указанных в списке правил номеров)]
     * @var string
     */
    protected $type;

    /**
     * @var BwlRuleUpdate
     */
    protected $rule;
}
