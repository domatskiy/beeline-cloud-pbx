<?php

namespace Domatskiy\BeelineCloudPBX\Data\Subscription;

class SubscriptionRequest
{
    /**
     * (string, optional): Идентификатор, входящий или добавочный номер абонента или номера
     * @var string|null
     */
    protected $pattern;

    /**
     * (number, optional): Длительность подписки
     * @var int|null
     */
    protected $expires;

    /**
     * (enum, optional): Тип подписки
     * BASIC_CALL (Базовая информация о вызове),
     * ADVANCED_CALL (Расширеная информация о вызове)
     * @var string|null
     */
    protected $subscriptionType;

    /**
     * @var string
     */
    protected $url;

    /**
     * @return string|null
     */
    public function getPattern(): ?string
    {
        return $this->pattern;
    }

    /**
     * @param string|null $pattern
     */
    public function setPattern(?string $pattern): void
    {
        $this->pattern = $pattern;
    }

    /**
     * @return int|null
     */
    public function getExpires(): ?int
    {
        return $this->expires;
    }

    /**
     * @param int|null $expires
     */
    public function setExpires(?int $expires): void
    {
        $this->expires = $expires;
    }

    /**
     * @return string|null
     */
    public function getSubscriptionType(): ?string
    {
        return $this->subscriptionType;
    }

    /**
     * @param string|null $subscriptionType
     */
    public function setSubscriptionType(?string $subscriptionType): void
    {
        $this->subscriptionType = $subscriptionType;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }
}
