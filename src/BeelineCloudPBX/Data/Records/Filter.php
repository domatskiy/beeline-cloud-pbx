<?php

namespace Domatskiy\BeelineCloudPBX\Data\Records;

class Filter
{
    /**
     * @var int|string|null
     */
    protected $userId;

    /**
     * @var string|null
     */
    protected $id;

    /**
     * @var \DateTime|null
     */
    protected $dateFrom;

    /**
     * @var \DateTime|null
     */
    protected $dateTo;

    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }

    /**
     * @param string|null $userId
     */
    public function setUserId(?string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateFrom(): ?\DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param \DateTime|null $dateFrom
     */
    public function setDateFrom(?\DateTime $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateTo(): ?\DateTime
    {
        return $this->dateTo;
    }

    /**
     * @param \DateTime|null $dateTo
     */
    public function setDateTo(?\DateTime $dateTo): void
    {
        $this->dateTo = $dateTo;
    }
}
