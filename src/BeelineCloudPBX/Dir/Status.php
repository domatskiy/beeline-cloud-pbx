<?php

namespace Domatskiy\BeelineCloudPBX\Dir;

class Status implements DirInterface
{
    const ONLINE = 'ONLINE';
    const OFFLINE = 'OFFLINE';
    const BREAK = 'BREAK';

    public static function getList():array
    {
        return [
            self::ONLINE => 'Агент подключён',
            self::OFFLINE => 'Агент отключён',
            self::BREAK => 'Перерыв',
        ];
    }
}
