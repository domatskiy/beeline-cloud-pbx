<?php

namespace Domatskiy\BeelineCloudPBX\Dir;

interface DirInterface
{
    /**
     * @return array
     */
    public static function getList():array;
}
