<?php

namespace Domatskiy\BeelineCloudPBX\Response;

class SubscriptionResult
{
    /**
     *  (string): Идентификатор подписки
     * @var string
     */
    protected $subscriptionId;

    /**
     * (number): Длительность подписки
     * @var int
     */
    protected $expires;
}
