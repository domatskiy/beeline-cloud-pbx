<?php

namespace Domatskiy\BeelineCloudPBX\Response;

class DirectRefResponse
{
    /**
     * (string): Прямая ссылка на файл записи разговора
     * @var string
     */
    protected $url;

    /**
     * (datetime (UTC)): Время жизни ссылки
     * @var int|\DateTime
     */
    protected $expirationDate;

    /**
     * (enum, optional): Тип файла по ссылке
     * WAV (Waveform Audio File Format (pcm_s16le, 8000 Hz, 1 channel)),
     * MP3 (MPEG Layer III)
     * @var string|null
     */
    protected $fileType;

    /**
     * (number, optional): Размер файла
     * @var int|null
     */
    protected $fileSize;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return \DateTime|int
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime|int $expirationDate
     */
    public function setExpirationDate($expirationDate): void
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string|null
     */
    public function getFileType(): ?string
    {
        return $this->fileType;
    }

    /**
     * @param string|null $fileType
     */
    public function setFileType(?string $fileType): void
    {
        $this->fileType = $fileType;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     */
    public function setFileSize(?int $fileSize): void
    {
        $this->fileSize = $fileSize;
    }
}
