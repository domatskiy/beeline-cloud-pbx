<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class IcrOperationError
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class IcrOperationError
{
    /**
     * (string): Код ошибки
     * @var string
     */
    protected $errorCode;

    /**
     *  (string): Сообщение об ошибке
     * @var string
     */
    protected $description;

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     */
    public function setErrorCode(string $errorCode): void
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
