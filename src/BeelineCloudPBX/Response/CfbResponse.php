<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class CfbResponse
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class CfbResponse
{
    /**
     * (enum): Статус переадресации = [ON (Переадресация включена), OFF (Переадресация выключена)]
     * @var string
     */
    protected $status;

    /**
     * (Cfb): Номера для переадресации
     * @var Cfb
     */
    protected $forward;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Cfb
     */
    public function getForward(): Cfb
    {
        return $this->forward;
    }

    /**
     * @param Cfb $forward
     */
    public function setForward(Cfb $forward): void
    {
        $this->forward = $forward;
    }
}
