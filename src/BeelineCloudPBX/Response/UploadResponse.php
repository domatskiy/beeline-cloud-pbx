<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class UploadResponse
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class UploadResponse
{
    /**
     * (string): Id файла, загруженного на сервер
     * @var string
     */
    protected $id;
}
