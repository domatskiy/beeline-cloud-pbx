<?php

namespace Domatskiy\BeelineCloudPBX\Response;

class NumberInfo
{
    /**
     *  (string): Идентификатор входящего номера
     * @var string
     */
    protected $numberId;

    /**
     *  (string): Номер телефона
     * @var string
     */
    protected $phone;

    /**
     * @return string
     */
    public function getNumberId(): string
    {
        return $this->numberId;
    }

    /**
     * @param string $numberId
     */
    public function setNumberId(string $numberId): void
    {
        $this->numberId = $numberId;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }
}
