<?php

namespace Domatskiy\BeelineCloudPBX\Response;

class SubscriptionInfo
{
    /**
     *  (string): Идентификатор подписки
     * @var string
     */
    protected $subscriptionId;

    /**
     * (enum): Тип объекта, для которого сформирована подписка
     * GROUP (События всей группы),
     * ABONENT (События абонента),
     * NUMBER (События номера)
     * @var string
     */
    protected $targetType;

    /**
     * (string): Идентификатор объекта, для которого сформирована подписка
     * @var string
     */
    protected $targetId;

    /**
     * (enum): Тип подписки
     * BASIC_CALL (Базовая информация о вызове),
     * ADVANCED_CALL (Расширеная информация о вызове)
     * @var string
     */
    protected $subscriptionType;

    /**
     * (number): Длительность подписки
     * @var int|string
     */
    protected $expires;

    /**
     * (string): URL приложения
     * @var string
     */
    protected $url;

    /**
     * @return string
     */
    public function getSubscriptionId(): string
    {
        return $this->subscriptionId;
    }

    /**
     * @param string $subscriptionId
     */
    public function setSubscriptionId(string $subscriptionId): void
    {
        $this->subscriptionId = $subscriptionId;
    }

    /**
     * @return string
     */
    public function getTargetType(): string
    {
        return $this->targetType;
    }

    /**
     * @param string $targetType
     */
    public function setTargetType(string $targetType): void
    {
        $this->targetType = $targetType;
    }

    /**
     * @return string
     */
    public function getTargetId(): string
    {
        return $this->targetId;
    }

    /**
     * @param string $targetId
     */
    public function setTargetId(string $targetId): void
    {
        $this->targetId = $targetId;
    }

    /**
     * @return string
     */
    public function getSubscriptionType(): string
    {
        return $this->subscriptionType;
    }

    /**
     * @param string $subscriptionType
     */
    public function setSubscriptionType(string $subscriptionType): void
    {
        $this->subscriptionType = $subscriptionType;
    }

    /**
     * @return int|string
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param int|string $expires
     */
    public function setExpires($expires): void
    {
        $this->expires = $expires;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }
}
