<?php

namespace Domatskiy\BeelineCloudPBX\Response;

class VoiceCampaign
{
    /**
     *  (string): Название
     * @var string
     */
    protected $name;

    /**
     * (enum): Текущее состояние =
     * PLANNED (Запланированна),
     * PROGRESS (Выполняется),
     * SUSPENDED (Приостановлена),
     * FINISHED (Завершена)
     *
     * @var string
     */
    protected $status;

    /**
     * (number): Идентификатор кампании
     * @var int
     */
    protected $recordId;

    /**
     * (enum): Тип кампании = [
     * MESSAGE (Голосовая кампания типа Сообщение),
     * QUESTION (Голосовая кампания типа Опрос)
     * ]
     * @var string
     */
    protected $type;

    /**
     * (Array [Answer], optional): Варианты ответа
     * @var array|null
     */
    protected $answers;

    /**
     * (string): Ссылка на аудио файл
     * @var string|null
     */
    protected $audioFile;

    /**
     * (Array [string]): Номера для обзвона
     * @var array
     */
    protected $phones;

    /**
     * (string): Исходящий номер телефона
     * @var string
     */
    protected $phoneNumber;

    /**
     * (VoiceCampaignSchedule): Правила проведения
     * @var VoiceCampaignSchedule
     */
    protected $schedule;

    /**
     * (DateAndTime): Период проведения с
     * @var int
     */
    protected $from;

    /**
     * (DateAndTime, optional): Период проведения по
     * @var int
     */
    protected $to;

    /**
     * (Abonent, optional): Абонент
     * @var Abonent
     */
    protected $abonent;
}
