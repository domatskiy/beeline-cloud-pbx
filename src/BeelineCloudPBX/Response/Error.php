<?php

namespace Domatskiy\BeelineCloudPBX\Response;

class Error
{
    /**
     * @var string|null
     */
    protected $errorCode; // example AbonentNotFound

    /**
     * @var string|null
     */
    protected $description; // Абонент не найден"
}
