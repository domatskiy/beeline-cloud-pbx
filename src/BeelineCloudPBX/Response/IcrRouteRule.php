<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class IcrRouteRule
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class IcrRouteRule
{
    /**
     *  (string): Входящий номер клиента
     * @var string
     */
    protected $inboundNumber;

    /**
     * (string): Внутренний номер
     * @var string
     */
    protected $extension;

    /**
     * @return string
     */
    public function getInboundNumber(): string
    {
        return $this->inboundNumber;
    }

    /**
     * @param string $inboundNumber
     */
    public function setInboundNumber(string $inboundNumber): void
    {
        $this->inboundNumber = $inboundNumber;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension(string $extension): void
    {
        $this->extension = $extension;
    }
}
