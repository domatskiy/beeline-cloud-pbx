<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class CfsStatusResponse
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class CfsStatusResponse
{
    /**
     * @var bool
     */
    protected $isCfsServiceEnabled;

    /**
     * @var CfsRule[]|null
     */
    protected $ruleList;

    /**
     * @return bool
     */
    public function isCfsServiceEnabled(): bool
    {
        return $this->isCfsServiceEnabled;
    }

    /**
     * @param bool $isCfsServiceEnabled
     */
    public function setIsCfsServiceEnabled(bool $isCfsServiceEnabled): void
    {
        $this->isCfsServiceEnabled = $isCfsServiceEnabled;
    }

    /**
     * @return CfsRule[]|null
     */
    public function getRuleList(): ?array
    {
        return $this->ruleList;
    }

    /**
     * @param CfsRule[]|null $ruleList
     */
    public function setRuleList(?array $ruleList): void
    {
        $this->ruleList = $ruleList;
    }
}
