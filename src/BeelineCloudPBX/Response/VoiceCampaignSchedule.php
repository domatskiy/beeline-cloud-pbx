<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class VoiceCampaignSchedule
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class VoiceCampaignSchedule
{
    /**
     * (enum): Если номер не отвечает или занят,
     * перезванивать n раз через 15 минут = [
     *   Q1 (1), Q2 (2), Q3 (3), Q4 (4), Q5 (5), Q6 (6), Q7 (7), Q8 (8), Q9 (9), Q10 (10)
     * ]
     * @var string
     */
    protected $tryQuantity;

    /**
     * (enum): Звонить с. Время приведено к UTC = [
     *  H0 (00:00), H1 (01:00), H2 (2:00), H3 (3:00), H4 (4:00), H5 (5:00), H6 (6:00), H7 (7:00), H8 (8:00), H9 (9:00),
     *  H10 (10:00), H11 (11:00), H12 (12:00), H13 (13:00), H14 (14:00), H15 (15:00), H16 (16:00), H17 (17:00),
     *  H18 (18:00), H19 (19:00), H20 (20:00), H21 (21:00), H22 (22:00), H23 (23:00)
     * ]
     * @var string
     */
    protected $fromHour;

    /**
     * (enum): Звонить до. Время приведено к UTC = [
     *  H0 (00:00), H1 (01:00), H2 (2:00), H3 (3:00), H4 (4:00), H5 (5:00), H6 (6:00), H7 (7:00), H8 (8:00), H9 (9:00),
     *  H10 (10:00), H11 (11:00), H12 (12:00), H13 (13:00), H14 (14:00), H15 (15:00), H16 (16:00), H17 (17:00),
     *  H18 (18:00), H19 (19:00), H20 (20:00), H21 (21:00), H22 (22:00), H23 (23:00)]
     * @var string
     */
    protected $toHour;

    /**
     * (enum): = [
     *      BUSINESS_DAY (По будням),
     *      WEEK_END (На выходных),
     *      ALL_WEEK (Всю неделю)
     * ]
     * @var string
     */
    protected $schedule;
}
