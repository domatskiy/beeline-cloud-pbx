<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class IcrRouteResult
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class IcrRouteResult
{
    /**
     *  (IcrRouteRule): Правило переадресации
     * @var IcrRouteRule
     */
    protected $rule;

    /**
     *  (enum): Результат выполнения операции = [SUCCESS (Успешно), FAULT (Ошибка)]
     * @var string
     */
    protected $status;

    /**
     * (IcrOperationError, optional): Описание ошибки
     * @var IcrOperationError|null
     */
    protected $error;

    /**
     * @return IcrRouteRule
     */
    public function getRule(): IcrRouteRule
    {
        return $this->rule;
    }

    /**
     * @param IcrRouteRule $rule
     */
    public function setRule(IcrRouteRule $rule): void
    {
        $this->rule = $rule;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return IcrOperationError|null
     */
    public function getError(): ?IcrOperationError
    {
        return $this->error;
    }

    /**
     * @param IcrOperationError|null $error
     */
    public function setError(?IcrOperationError $error): void
    {
        $this->error = $error;
    }
}
