<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class Answer
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class Answer
{
    /**
     * (enum): Выбор = [
     *   B_1 (1),
     *   B_2 (2),
     *   B_3 (3),
     *   B_4 (4),
     *   B_5 (5),
     *   B_6 (6),
     *   B_7 (7), B_8 (8), B_9 (9), B_S (*), B_0 (0), B_D (#)]
     * @var string
     */
    protected $choice;

    /**
     * (string): Вариант ответа
     * @var string
     */
    protected $answer;
}
