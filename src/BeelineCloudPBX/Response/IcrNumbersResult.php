<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class IcrNumbersResult
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class IcrNumbersResult
{
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_FAULT = 'FAULT';

    /**
     *  (string): Номер телефона
     * @var string|null
     */
    protected $phoneNumber;

    /**
     *  (enum): Результат выполнения операции = [SUCCESS (Успешно), FAULT (Ошибка)]
     * @var string
     */
    protected $status;

    /**
     * (IcrOperationError, optional): Описание ошибки
     * @var IcrOperationError|null
     */
    protected $error;
}
