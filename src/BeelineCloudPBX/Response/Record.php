<?php

namespace Domatskiy\BeelineCloudPBX\Response;

class Record
{
    // Входящий вызов
    const DIRECTION_INBOUND = 'INBOUND';

    // Исходящий вызов
    const DIRECTION_OUTBOUND = 'OUTBOUND';

    /**
     * Идентификатор записи
     * @var string
     */
    protected $id;

    /**
     * Внешний идентификатор записи
     * @var string
     */
    protected $externalId;

    /**
     * Мобильный номер абонента
     * @var string|null
     */
    protected $phone;

    /**
     *  (enum)Тип вызова = [INBOUND (Входящий вызов), OUTBOUND (Исходящий вызов)]
     * @var string
     */
    protected $direction;

    /**
     * Дата и время разговора
     * @var int
     */
    protected $date;

    /**
     *  (number): Длительность разговора в миллисекундах
     * @var int
     */
    protected $duration;

    /**
     * Размер файла записи разговора
     * @var int
     */
    protected $fileSize;

    /**
     * Комментарий к записи разговора
     * @var string
     */
    protected $comment;

    /**
     * Абонент
     * @var Abonent
     */
    protected $abonent;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     */
    public function setDirection(string $direction): void
    {
        $this->direction = $direction;
    }

    /**
     * @return int
     */
    public function getDate(): int
    {
        return $this->date;
    }

    /**
     * @param int $date
     */
    public function setDate(int $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return int
     */
    public function getFileSize(): int
    {
        return $this->fileSize;
    }

    /**
     * @param int $fileSize
     */
    public function setFileSize(int $fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return Abonent
     */
    public function getAbonent(): Abonent
    {
        return $this->abonent;
    }

    /**
     * @param Abonent $abonent
     */
    public function setAbonent(Abonent $abonent): void
    {
        $this->abonent = $abonent;
    }
}
