<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class Cfb
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class Cfb
{
    /**
     *  (string, optional)
     * @var string|null
     */
    protected $forwardAllCallsPhone;

    /**
     *  (string, optional)
     * @var string|null
     */
    protected $forwardBusyPhone;

    /**
     *  (string, optional)
     * @var string|null
     */
    protected $forwardUnavailablePhone;

    /**
     * Номер, на который будет выполнена переадресация если номер не отвечает
     * (string, optional)
     * @var string|null
     */
    protected $forwardNotAnswerPhone;

    /**
     * Колличество гудков, которые необходимо подождать для ответа номера
     * (string, optional)
     * @var int|null
     */
    protected $forwardNotAnswerTimeout;

    /**
     * @return string|null
     */
    public function getForwardAllCallsPhone(): ?string
    {
        return $this->forwardAllCallsPhone;
    }

    /**
     * @param string|null $forwardAllCallsPhone
     */
    public function setForwardAllCallsPhone(?string $forwardAllCallsPhone): void
    {
        $this->forwardAllCallsPhone = $forwardAllCallsPhone;
    }

    /**
     * @return string|null
     */
    public function getForwardBusyPhone(): ?string
    {
        return $this->forwardBusyPhone;
    }

    /**
     * @param string|null $forwardBusyPhone
     */
    public function setForwardBusyPhone(?string $forwardBusyPhone): void
    {
        $this->forwardBusyPhone = $forwardBusyPhone;
    }

    /**
     * @return string|null
     */
    public function getForwardUnavailablePhone(): ?string
    {
        return $this->forwardUnavailablePhone;
    }

    /**
     * @param string|null $forwardUnavailablePhone
     */
    public function setForwardUnavailablePhone(?string $forwardUnavailablePhone): void
    {
        $this->forwardUnavailablePhone = $forwardUnavailablePhone;
    }

    /**
     * @return string|null
     */
    public function getForwardNotAnswerPhone(): ?string
    {
        return $this->forwardNotAnswerPhone;
    }

    /**
     * @param string|null $forwardNotAnswerPhone
     */
    public function setForwardNotAnswerPhone(?string $forwardNotAnswerPhone): void
    {
        $this->forwardNotAnswerPhone = $forwardNotAnswerPhone;
    }

    /**
     * @return int|null
     */
    public function getForwardNotAnswerTimeout(): ?int
    {
        return $this->forwardNotAnswerTimeout;
    }

    /**
     * @param int|null $forwardNotAnswerTimeout
     */
    public function setForwardNotAnswerTimeout(?int $forwardNotAnswerTimeout): void
    {
        $this->forwardNotAnswerTimeout = $forwardNotAnswerTimeout;
    }
}
