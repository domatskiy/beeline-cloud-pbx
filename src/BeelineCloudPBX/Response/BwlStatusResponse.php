<?php

namespace Domatskiy\BeelineCloudPBX\Response;

/**
 * Class BwlStatusResponse
 * @package Domatskiy\BeelineCloudPBX\Response
 */
class BwlStatusResponse
{
    /**
     * (enum) = [
     *  BLACK_LIST_ON (Не принимать звонки с указанных в списке правил номеров),
     *  WHITE_LIST_ON (Принимать звонки только с указанных в списке правил номеров),
     *  OFF (Услуга отключена)
     * ]
     * @var string
     */
    protected $status;

    /**
     * (Array [BwlRule], optional)
     * @var BwlRule[]|null
     */
    protected $blackList;

    /**
     * (Array [BwlRule], optional)
     * @var BwlRule[]|null
     */
    protected $whiteList;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return BwlRule[]|null
     */
    public function getBlackList(): ?array
    {
        return $this->blackList;
    }

    /**
     * @param BwlRule[]|null $blackList
     */
    public function setBlackList(?array $blackList): void
    {
        $this->blackList = $blackList;
    }

    /**
     * @return BwlRule[]|null
     */
    public function getWhiteList(): ?array
    {
        return $this->whiteList;
    }

    /**
     * @param BwlRule[]|null $whiteList
     */
    public function setWhiteList(?array $whiteList): void
    {
        $this->whiteList = $whiteList;
    }
}
