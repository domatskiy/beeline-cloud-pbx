<?php

namespace Domatskiy\BeelineCloudPBX;

use Domatskiy\BeelineCloudPBX\Exception\ResponseClassException;
use Exception;
use GuzzleHttp\HandlerStack;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class Client
 * @package Domatskiy\BeelineCloudPBX
 */
abstract class Client
{
    protected $host = 'https://cloudpbx.beeline.ru';
    protected $token = null;
    protected $handler;

    /**
     * @desc http://docs.guzzlephp.org/en/stable/quickstart.html
     * @var string
     */
    protected $base_uri = '/apis/portal/';

    /**
     * @var string
     */
    protected $lastResponseContent;

    /**
     * @var array
     */
    protected $options = [
        'timeout'  => 30.0,
        'verify' => false,
        'headers' => []
    ];

    /**
     * Client constructor.
     * @param string|null $token
     * @throws Exception
     */
    public function __construct(?string $token = null)
    {
        if (!is_string($this->base_uri)) {
            throw new Exception('not correct base url');
        }

        if (!is_scalar($token)) {
            throw new Exception('not correct token');
        }

        if ($this->host !== null && !is_string($this->host)) {
            throw new Exception('not correct host');
        }

        $this->token = $token;

        $url = str_replace('//', '/', $this->base_uri);
        $url = $this->host.$url;

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new Exception('not correct url: '.$url);
        }

        $this->options['base_uri'] = $url;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getBaseUrl():string
    {
        if (!isset($this->options['base_uri'])) {
            throw new Exception('option "base_uri" not found');
        }

        return $this->options['base_uri'];
    }

    /**
     * @param HandlerStack $handler
     * @return Client
     */
    public function setHandler(HandlerStack $handler):Client
    {
        $this->handler = $handler;

        return $this;
    }

    /**
     * @param array $headers
     *
     * @return $this
     * @throws Exception
     */
    public function setHeaders(array $headers):Client
    {
        foreach ($headers as $key => $val) {
            if (!is_string($val)) {
                throw new Exception(sprintf('not correct header value for %s', $key));
            }
        }

        $this->options['headers'] = $headers;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastResponseContent():string
    {
        return $this->lastResponseContent;
    }

    /**
     * @param string $key
     * @param string $value
     * @return Client
     * @throws Exception
     */
    public function setHeader(string $key, string $value):Client
    {
        if (!strlen($key)) {
            throw new Exception('empty header key');
        }

        if (!strlen($value)) {
            throw new Exception('empty header value for '.$key);
        }

        $this->options['headers'][$key] = $value;

        return $this;
    }

    /**
     * @param int|null $timeout
     * @return Client
     * @throws Exception
     */
    public function setTimeout(?int $timeout):Client
    {
        if (!is_numeric($timeout) || $timeout < 1) {
            throw new Exception('not correct timeout');
        }

        $this->options['timeout'] = $timeout;

        return $this;
    }

    /**
     * @param string $url
     * @param object|array|string $params
     * @param string $responseType
     * @return mixed
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function get(string $url, $params, ?string $responseType)
    {
        if (is_object($params)) {
            $serializer = $this->getSerializer();
            $params = $serializer->normalize($params);
        }

        if (is_array($params)) {
            $params = http_build_query($params);
        }

        if (!is_string($params)) {
            throw new Exception('not correct params');
        }

        return $this->request('get', $url, ['query' => $params], $responseType);
    }

    /**
     * @param string $url
     * @param object|array $data
     * @param string $responseType
     * @return mixed
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function post(string $url, $data, ?string $responseType)
    {
        if (is_object($data)) {
            $serializer = $this->getSerializer();
            $data = $serializer->normalize($data);
        }

        if (!is_array($data)) {
            throw new Exception('not correct data, need array or object');
        }

        return $this->request('post', $url, ['form_params' => $data], $responseType);
    }

    /**
     * @param string $url
     * @param object|array $data
     * @param string $responseType
     * @return mixed
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function put(string $url, $data, ?string $responseType)
    {
        if (is_object($data)) {
            $serializer = $this->getSerializer();
            $data = $serializer->normalize($data);
        }

        if (!is_array($data)) {
            throw new Exception('not correct data, need object or array');
        }

        return $this->request('put', $url, ['form_params' => $data], $responseType);
    }

    /**
     * @param string $url
     * @param mixed $data
     * @param string|null $responseType
     * @return array|object
     * @throws ResponseClassException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function delete(string $url, $data, ?string $responseType = null)
    {
        if (is_object($data)) {
            $serializer = $this->getSerializer();
            $data = $serializer->normalize($data);
        }

        if (!is_array($data)) {
            throw new Exception('not correct data, need object or array');
        }

        return $this->request('delete', $url, $data, $responseType);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @param string|null $responseType
     * @return array|object
     * @throws ResponseClassException
     */
    protected function request(string $method, string $uri, array $options, ?string $responseType)
    {
        if ($responseType !== null && strlen($responseType) < 1) {
            throw new ResponseClassException(sprintf('not correct response type for request %s %s', $method, $uri));
        }

        $client = $this->getClient($options);

        $requestOpt = [];

        if ($this->token) {
            # add token
            $requestOpt['headers']['X-MPBX-API-AUTH-TOKEN'] = $this->token;
        }

        // $requestOpt['headers']['client-version'] = self::$version;

        $response = $client->request($method, $uri.'', $requestOpt);

        $this->lastResponseContent = $response->getBody()->getContents();
        $contentType = $response->getHeader('content-type');

        # check content-type: application/json;charset=utf-8
        if ($responseType && isset($contentType[0]) && strpos($contentType[0], 'application/json') !== false) {
            $serializer = $this->getSerializer();
            $resultResponse = $serializer->deserialize($this->lastResponseContent, $responseType, 'json');
        } else {
            # content-type: application/octet-stream;charset=utf-8
            $resultResponse = $this->lastResponseContent;
        }

        /**
         * @var $resultResponse object|array|string
         */
        return $resultResponse;
    }

    /**
     * @return Serializer
     */
    protected function getSerializer():Serializer
    {
        // normalizers
        $normalizers = [
            new DateTimeNormalizer(),
            new ArrayDenormalizer(),
            new ObjectNormalizer(null, null, new PropertyAccessor(), new PhpDocExtractor()),
        ];

        //encoders
        $encoders = [
            new JsonEncoder()
        ];

        return new Serializer($normalizers, $encoders);
    }

    /**
     * @param array $options
     * @return \GuzzleHttp\Client
     */
    protected function getClient(array $options = []):\GuzzleHttp\Client
    {
        if ($this->handler) {
            $options['handler'] = $this->handler;
        }

        // объединяем настройки
        $options = array_merge($this->options, $options);

        // create GuzzleHttp Client
        return new \GuzzleHttp\Client($options);
    }
}
