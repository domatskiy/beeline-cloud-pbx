<?php

namespace Domatskiy;

use Domatskiy\BeelineCloudPBX\Dir\Status;
use Domatskiy\BeelineCloudPBX\Exception\InvalidArgumentException;
use Domatskiy\BeelineCloudPBX\Response;
use Domatskiy\BeelineCloudPBX\Client;
use Domatskiy\BeelineCloudPBX\Data;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class BeelineCloudPBX
 * @package Domatskiy
 */
class BeelineCloudPBX extends Client
{
    /**
     * Возвращает список всех абонентов
     *
     * @return Response\Abonent[]
     * @throws ExceptionInterface
     */
    public function getAbonents():array
    {
        return $this->get('abonents', [], Response\Abonent::class.'[]');
    }

    /**
     * Ищет абонента по идентификатору, мобильному или добавочному номеру
     *
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @return Response\Abonent
     * @throws ExceptionInterface
     */
    public function getAbonent(string $patten): Response\Abonent
    {
        return $this->get("abonents/$patten", [], Response\Abonent::class);
    }

    /**
     * Возвращает статус агента call-центра
     *
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @return string - Возвращаемое значение: enum :
     *  ONLINE (Агент подключён),
     *  OFFLINE (Агент отключён),
     *  BREAK (Перерыв)
     *
     * @throws ExceptionInterface
     */
    public function getAbonentAgent(string $patten): string
    {
        return $this->get("abonents/$patten/agent", [], null);
    }

    /**
     * Устанавливает статус агента call-центра
     *
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @param string $status - ONLINE (Агент подключён), OFFLINE (Агент отключён), BREAK (Перерыв)
     * @return mixed
     * @throws ExceptionInterface|InvalidArgumentException
     */
    public function setAbonentAgent(string $patten, string $status)
    {
        if (!array_key_exists($status, Status::getList())) {
            throw new InvalidArgumentException(sprintf('not correct status "%s" for %s', $status, $patten));
        }

        return $this->put("abonents/$patten/agent", [], null);
    }

    /**
     * Возвращает статус записи разговоров для абонента
     *
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @return string - Возвращаемое значение: enum : [ON (Запись включена), OFF (Запись выключена)]
     * @throws ExceptionInterface
     */
    public function getAbonentRecording(string $patten): string
    {
        return $this->get("abonents/$patten/recording", [], null);
    }

    /**
     * Включает запись разговоров для абонента
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @return string
     * @throws ExceptionInterface
     */
    public function setAbonentRecording(string $patten)
    {
        return $this->put("abonents/$patten/recording", [], null);
    }

    /**
     * Отключает запись разговоров для абонента
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @return array|object
     * @throws BeelineCloudPBX\Exception\ResponseClassException
     * @throws ExceptionInterface
     */
    public function delAbonentRecording(string $patten)
    {
        return $this->delete("abonents/$patten/recording", [], null);
    }

    /**
     * Совершает звонок от имени абонента
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @param string $phoneNumber
     * @return string - Звонок выполнен, в ответе ID звонка
     * @throws ExceptionInterface
     * @deprecated
     */
    public function getAbonentCall(string $patten, string $phoneNumber):string
    {
        if (strlen($phoneNumber) <> 10) {
            throw new Exception('not correct phone number');
        }

        return $this->post("abonents/$patten/call", ['phoneNumber' => $phoneNumber], null);
    }

    /**
     * Совершает звонок от имени абонента
     * @param string $patten - Идентификатор, мобильный или добавочный номер абонента
     * @param string $phoneNumber
     * @return string - Звонок выполнен, в ответе ID звонка
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function postAbonentCall(string $patten, string $phoneNumber): string
    {
        if (strlen($phoneNumber) <> 10) {
            throw new Exception('not correct phone number '.$phoneNumber);
        }

        return $this->post("abonents/$patten/call", ['phoneNumber' => $phoneNumber], null);
    }

    /**
     * @param Data\Records\Filter $filter
     * @return Response\Record[]
     * @throws ExceptionInterface
     */
    public function getRecords(Data\Records\Filter $filter):array
    {
        return $this->get('records', $filter, Response\Record::class.'[]');
    }

    /**
     * @param int $recordId
     * @return Response\Record
     * @throws ExceptionInterface
     */
    public function getRecord(int $recordId): Response\Record
    {
        return $this->get("v2/records/$recordId", '', Response\Record::class);
    }

    /**
     * @param int $recordId
     * @return Response\Record
     * @throws BeelineCloudPBX\Exception\ResponseClassException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function deleteRecord(int $recordId): Response\Record
    {
        return $this->delete('v2/records/'.$recordId, '');
    }

    /**
     * @param int $extTrackingId
     * @param int $userId
     * @return Response\Record
     * @throws ExceptionInterface
     */
    public function getRecordByTracking(int $extTrackingId, int $userId): Response\Record
    {
        return $this->get("records/$extTrackingId/$userId", '', Response\Record::class);
    }

    /**
     * Возвращает файл записи разговора
     * @param int $recordId
     * @return string
     * @throws ExceptionInterface
     */
    public function getRecordFile(int $recordId): string
    {
        /**
         * response:
         * content-disposition: attachment; filename=file.mp3
         * content-length: 40968
         * content-type: application/octet-stream;charset=utf-8
         */
        return $this->get("v2/records/$recordId/download", '', null);
    }

    /**
     * Возвращает прямую ссылку на файл с записью разговора
     * @param int $recordId
     * @return Response\DirectRefResponse
     * @throws ExceptionInterface
     */
    public function getRecordLink(int $recordId): Response\DirectRefResponse
    {
        return $this->get("records/$recordId/reference", '', Response\DirectRefResponse::class);
    }

    /**
     * Формирует подписку на Xsi-Events
     *
     * Подписка может быть использована для интеграции со сторонними системами, которым необходим контроль над
     * звонками абонентов облачной АТС в реальном времени. API использует механизм подписки на события,
     * ассоциированные с тем или иным абонентом, номером или всем клиентом.
     * Например, Абонент облачной АТС принимает вызов, сторонняя CRM система получает обновления о текущем статусе
     * вызова (ringing, established, completed).
     *
     * @param Data\Subscription\SubscriptionRequest $req
     * @return Response\SubscriptionResult
     * @throws ExceptionInterface
     */
    public function addSubscription(Data\Subscription\SubscriptionRequest $req): Response\SubscriptionResult
    {
        return $this->put('subscription', $req, Response\SubscriptionResult::class);
    }

    /**
     * Возвращает информацию о подписке на Xsi-Events
     * @param int $subscriptionId
     * @return Response\SubscriptionInfo
     * @throws ExceptionInterface
     */
    public function getSubscription(int $subscriptionId): Response\SubscriptionInfo
    {
        return $this->get("subscription/$subscriptionId", '', Response\SubscriptionInfo::class);
    }

    /**
     * Отключает подписку на Xsi-Events
     *
     * @param int $subscriptionId
     * @return array|object
     * @throws BeelineCloudPBX\Exception\ResponseClassException
     * @throws ExceptionInterface
     */
    public function deleteSubscription(int $subscriptionId)
    {
        return $this->delete("subscription/$subscriptionId", [], null);
    }

    /**
     * Возвращает список всех входящих номеров
     * @return Response\NumberInfo[]
     * @throws ExceptionInterface
     */
    public function getNumbers(): array
    {
        return $this->get('numbers', '', Response\NumberInfo::class.'[]');
    }

    /**
     * Ищет входящий номер по идентификатору или номеру
     * @param string $pattern
     * @return Response\NumberInfo
     * @throws ExceptionInterface
     */
    public function getNumber(string $pattern): Response\NumberInfo
    {
        return $this->get("numbers/$pattern", '', Response\NumberInfo::class);
    }

    /**
     * Возвращает список входящих номеров, для которых включена переадресация
     * @return Response\NumberInfo[]
     * @throws ExceptionInterface
     */
    public function getIcrNumbers(): array
    {
        return $this->get('icr/numbers', '', Response\NumberInfo::class.'[]');
    }

    /**
     * Включает индивидуальную переадресацию для входящих номеров
     * @param array $phones - example ["9998883001", "9998883002", "9998883003"]
     * @return Response\IcrNumbersResult[]
     * @throws ExceptionInterface
     */
    public function setIcrNumbers(array $phones): array
    {
        return $this->put('icr/numbers', $phones, Response\IcrNumbersResult::class.'[]');
    }

    /**
     * Отключает индивидуальную переадресацию для входящих номеров
     *
     * @return array|object
     * @throws BeelineCloudPBX\Exception\ResponseClassException
     * @throws ExceptionInterface
     */
    public function deleteIcrNumbers()
    {
        return $this->delete('icr/numbers', [], Response\IcrNumbersResult::class.'[]');
    }

    /**
     * Возвращает список правил переадресации
     *
     * @return Response\IcrRouteRule[]
     * @throws ExceptionInterface
     */
    public function getIcrRoute(): array
    {
        return $this->get('icr/route', '', Response\IcrRouteRule::class.'[]');
    }

    /**
     * Замещает правила переадресации
     *
     * @param Response\IcrRouteRule[] $IcrRouteRule
     * @return Response\IcrRouteResult[]
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function replaceIcrRoute(array $IcrRouteRule): array
    {
        foreach ($IcrRouteRule as $item) {
            if (!($item instanceof Response\IcrRouteRule)) {
                throw new Exception('$IcrRouteRule must be instance of IcrRouteRule');
            }
        }

        return $this->post('icr/route', [], Response\IcrRouteResult::class.'[]');
    }

    /**
     * Объединяет правила переадресации
     *
     * @param Response\IcrRouteRule[] $IcrRouteRule
     * @return Response\IcrRouteResult[]
     * @throws ExceptionInterface
     */
    public function joinIcrRoute(array $IcrRouteRule): array
    {
        foreach ($IcrRouteRule as $item) {
            if (!($item instanceof Response\IcrRouteRule)) {
                throw new Exception('$IcrRouteRule must be instance of IcrRouteRule');
            }
        }

        return $this->put('icr/route', [], Response\IcrRouteResult::class.'[]');
    }

    /**
     * Удаляет список правил переадресации
     *
     * @param array $data
     * @return Response\IcrRouteResult[]
     * @throws BeelineCloudPBX\Exception\ResponseClassException
     * @throws ExceptionInterface
     */
    public function deleteIcrRoute(array $data): array
    {
        return $this->delete('icr/route', $data, Response\IcrRouteResult::class.'[]');
    }

    /**
     * Возвращает список всех голосовых компаний
     *
     * @return Response\VoiceCampaign[]
     * @throws ExceptionInterface
     */
    public function getVc(): array
    {
        return $this->get('vc', '', Response\VoiceCampaign::class.'[]');
    }

    public function getStat()
    {
        // https://cloudpbx.beeline.ru/apis/portal/statistics?userId=9600489131%40ip.beeline.ru&dateFrom=2021-08-16T00%3A00%3A00.000Z&dateTo=2021-08-26T00%3A00%3A00.000Z&page=1&pageSize=20
    }
}
