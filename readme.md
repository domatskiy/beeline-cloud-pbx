## Beeline Cloud PBX

### install 

```
composer require domatskiy/beeline-cloud-pbx
```

### USE

##### get abonents
```php
$Client = new BeelineCloudPBX(<token>);
$Abonents = $Client->getAbonents();
foreach ($Abonents as $abonent) {
    echo $abonent->getPhone();
}
```

##### get records
```php
$Client = new BeelineCloudPBX(<token>);

// records filter
$filter = new BeelineCloudPBX\Data\Records\Filter();

$result = $Client->getRecords($filter);
foreach ($result as $record) {
    echo $record->getId();
}
```

##### get file
```php
$Client = new BeelineCloudPBX(<token>);

$file = $Client->getRecordFile(<id>);
```

##### get link to file
```php
$Client = new BeelineCloudPBX(<token>);

$link = $Client->getRecordLink(<id>);
echo $link->getUrl();
```
